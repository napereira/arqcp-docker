FROM i386/ubuntu:16.04

# install some tools
RUN apt-get update && \
    apt-get -y install build-essential nasm gdb gdbserver git duff locales && \
    apt-get clean && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/*

ENV LANG en_GB.UTF-8
ENV LANG en_US.UTF-8
RUN locale-gen en_US.UTF-8 

WORKDIR /projects

CMD tail -f /dev/null