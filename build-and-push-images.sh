#!/bin/bash
### Build docker images for ubuntu i386 with tools for arqcp and codenvy dependencies.
### Note: must do "docker login" before running this script
### https://hub.docker.com/r/npereira/arqcp


# build image with arqcp tools (build-essential, nasm, gdb, git, ...)
docker build --tag npereira/arqcp:1819 .

# push to dockerhub (also push ":latest" tag); do "docker login" before
docker push npereira/arqcp:1819
docker tag npereira/arqcp:1819 npereira/arqcp:latest
docker push npereira/arqcp:latest

# to run a bash in this container: 
#		docker run --name arqcp-container --rm -it -v $PWD:/arqcp npereira/arqcp bash


# build image with additional codenvy dependencies (java, rsync, sshd, curl, ...)
docker build --tag npereira/arqcp:codenvy -f Dockerfile-codenvy .

# push do dockerhub; do "docker login" before
docker push npereira/arqcp:codenvy

# to run a bash in this container: 
#		docker run --name arqcp-container --rm -it -v $PWD:/arqcp npereira/arqcp:codenvy bash
