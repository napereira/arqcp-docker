**Docker Images with 32-bit Ubuntu for the Computer Architecture (ARQCP) classes.**

These are the scripts used to create the ARQCP docker images available at https://hub.docker.com/r/npereira/arqcp/.

These images can be used to perform the activities of the ARQCP course (lab classes, assignments, etc).

There are 2 images:

1. The image with the tools (build-essential, nasm, gdb, git, ...) needed for ARQCP
2. The image with the dependencies needed to run in [Codenvy](https://codenvy.io) workspaces, with the tag **codenvy**

---

## The ARQCP Image (**npereira/arqcp**)

This image **is the one you should normally use for the activities during the ARQCP course**, for example, to compile and debug your code in your personal computer. The way to use this image is to run a container that shares the files from your source code repository in your computer. 

The following example runs a command prompt (bash) in a container. You can execute this inside your code repository on your machine to have it shared with the container (the code will be inside the **/arqcp** folder in the container):

```
docker run --name arqcp-container --rm -it -v ${PWD}:/arqcp npereira/arqcp bash
```

**Note for Windows users**: in the Windows Command Line (cmd), replace `${PWD}`by `%cd%`. In PowerShell, you can use `${PWD}`.

---

## The Codenvy Image (**npereira/arqcp:codenvy**)

This image can be used to create a new single-machine stack in [Codenvy](https://codenvy.io). Using this stack, you can then create a workspace that you can use to develop, compile and debug your code using only the browser! Here are the steps to do it.

#### 1. Login/Create an account at [Codenvy](https://codenvy.io) 

#### 2. Create a new stack
On the [dashboard main page](https://codenvy.io/dashboard), select "Stacks" on the left menu, which shlould bring you to [the availables Stacks](https://codenvy.io/dashboard/#/stacks). Here, press the "Build Stack from Recipe" and enter the folowing recipe:

```
services:
 dev-machine:
  image: npereira/arqcp:codenvy
```

Then, enter the name:**"ARQCP Stack"**, then enter a description, remove the tags and press "Save".


#### 3. Create a new workspace using the ARQCP Stack 

Select "Workspaces" on the left menu, which will bring you to your [Workspaces](https://codenvy.io/dashboard/#/stacks). The list of Workspaces should be empty if this is your first time using Codenvy. Let us create a new Workspace by pressing "Add Workspace".

Now, enter a name for the Workspace: "ARQCPWorkspace". Then select the Stack you just created (**ARQCP Stack**). You can just "Create" or **take the chance to create a project from your ARQCP code repository in bitbucket** (for this, choose GIT and enter the URL used to clone your repository using HTTP).

After pressing create, the Workspace will be provisioned and after a while, you will have your environment ready. On the bottom, you will have a command prompt where you can navigate within the code and compile (gcc, make) and debug (gdb).



